/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [],
  theme: {
    extend: {
      colors: {
        "pacific-green": "#07BE92",
        "strong-blue": "#00455C",
        "light-gray": "#F8F8F8",
        "md-gray": "#A8B5B7"
      },
    },
  },
  plugins: [],
};
