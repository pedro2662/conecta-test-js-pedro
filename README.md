# conecta-test-js-pedro

Prueba técnica por Pedro De León Bahena

## Instalación

Para correr el proyecto de forma local se necesita instalar las dependencias con el comando.

```bash
npm install
```

En caso de tener problemas con el diseño eliminar el archivo tailwind.css y ejecutar el comando

```bash
npx tailwindcss-cli@latest build -o public/styles/tailwind.css
```

## Correr el proyecto

Para ejecutar el proyecto en modo desarrollador se utiliza el comando

```bash
npm run start
```

Ejecutará el proyecto en el puerto 4000
