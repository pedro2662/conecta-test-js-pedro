import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import path from "path";
import { fileURLToPath } from 'url';
import home from "./functions/render-home.js";
// Create express app
const app = express();
const port = process.env.PORT || 4000;
// Requests of content-type
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// Cors
app.use(cors());
// Pug views
app.set("views", "./views");
app.set("view engine", "pug");
// Public directory
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
app.use(express.static(path.join(__dirname, "public")));
// Routes
app.get("/", (req, res) => {
  home(req, res);
});

// listen for requests
app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});
