import { fetchData } from "../services/account.service.js";

const home = async (req, res) => {
  const { cuenta } = (await fetchData("cuenta")) || [];
  const { saldos } = (await fetchData("saldos")) || [];
  const { tarjetas } = (await fetchData("tarjetas")) || [];
  const { movimientos } = (await fetchData("movimientos")) || [];

  res.render("home.pug", {
    cuenta: cuenta && cuenta[0],
    saldos: saldos && saldos[0],
    tarjetas,
    movimientos,
  });
};

export default home;
