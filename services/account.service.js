import fetch from "node-fetch";

const api = "http://bankapp.endcom.mx/api/bankappTest";

export const fetchData = async (endpoint) => {
  try {
    const data = await fetch(`${api}/${endpoint}`);
    return await data.json();
  } catch (e) {
    console.log("Something's wrong in account service", e);
  }
};
